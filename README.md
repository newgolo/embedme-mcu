# embedme-mcu

#### 介绍
本项目包含了51单片机的protothread工程，STM32F10x系列的CoOS,uCOSII,klite等实时操作系统工程。源码中还包含了常用的i2c,spi，uart, rtc等设备的驱动程序，可用于快速构建单片机项目，进行板级验证和快速完成软件功能的实现。本源码基于MIT协议，请遵照该协议进行使用和分发。

# 作者
有任何问题，欢迎联系: cblock@126.com

#### 软件架构
软件架构说明
##### 1.protothread项目基于protothread v1.4版本,target为51单片机
##### 2.CoOS项目基于116版本,target为stm32f10x系列单片机
##### 3.uCOS项目基于286版本,target为stm32f10x系列单片机
##### 4.klite项目基于2010001版本,target为stm32f10x系列单片机