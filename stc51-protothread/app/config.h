/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __CONFIG_H__
#define __CONFIG_H__

#define SYSTEM_TICK_MS  10
#define CPU_MIPMS       120
#define CPU_MIPUS       1
/* 根据系统资源及需求确定模块个数 */
#define TIMER_COUNT         (2)   /* 定义定时器个数*/
#define UART_COUNT          (1)   /* 串口个数,51单片机只有一个 */

/* 特定MCU功能的控制宏 */
#define MCU_STC12LE5608     0
#endif