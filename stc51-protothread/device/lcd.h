/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LCD_H__
#define __LCD_H__
#include "basetype.h"

void lcd_init(void);
void lcd_clear(void);
void lcd_draw_chr(uint8 chr, uint8 x, uint8 y);
void lcd_draw_wchr(uint16 wchr, uint8 x, uint8 y);
void lcd_draw_text(uint8 x, uint8 y, const char* text);
void lcd_draw_cursor(uint8 x, uint8 y);

#endif