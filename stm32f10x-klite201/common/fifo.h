/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __FIFO_H__
#define	__FIFO_H__

#include "basetype.h"

typedef struct{
    char* start;    /* 可用的缓冲区起始地址 */
    char* end;		/* 缓冲区结束地址 */
    char* in;		/* 数据入口指针 */
    char* out;		/* 数据出口指针 */
    uint32 size;	/* 可用的缓冲区大小 */
    uint32 entry;	/* 进入缓冲区中的总数据 */
}fifo_s;

bool fifo_init(void* pfifo, uint32 size);
bool fifo_putchar(void* pfifo, char ch);
uint32 fifo_putbuf(void* pfifo, char* pbuf, uint32 len);
uint32 fifo_putstr(void* pfifo, char* pstr);
uint32 fifo_getchar(void* pfifo, char* pch);
uint32 fifo_getbuf(void* pfifo, char* pbuf, uint32 len);
uint32 fifo_getline(void* pfifo, char* pbuf, uint32 size);
void fifo_flush(void* pfifo);
#endif

