/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "basetype.h"
#include "board.h"
#include "kernel.h"
#include "uart.h"
#include "system.h"
#include "trace.h"
#include "fifo.h"
#include "gpio.h"

#define RXBUF_SIZE      64      /* 设置接收缓存大小为64字节 */
typedef struct{
    USART_TypeDef* m_usart;
    uint16 m_rxbufLen;
    uint16 m_txbufLen;
    char* m_rxbuf;
    char* m_txbuf;
}UartInfo_S;

static UartInfo_S s_uartInfo[UART_MAX]={
#if UART_MAX>0
    {USART1,0,0,NULL,NULL},
#endif
#if UART_MAX>1
    {USART2,0,0,NULL,NULL},
#endif
#if UART_MAX>2
    {USART3,0,0,NULL,NULL}
#endif
};

#if UART_MAX>0
static char s_uart0_rxbuf[1024 + sizeof(fifo_s)];/* UART_COM0:用作打印串口 */
#endif
#if UART_MAX>1
static char s_uart1_rxbuf[1024+ sizeof(fifo_s)]; /* UART_COM1:用作手咪转接板通信 */
#endif
#if UART_MAX>2
static char s_uart2_rxbuf[512+ sizeof(fifo_s)];  /* UART_COM2:用作录音模块通信 */
#endif

#if USE_UART_TX_INT
static OS_EVENT* s_semUartTxReady[UART_MAX];
#if UART_MAX>0
static char s_uart0_txbuf[64+ sizeof(fifo_s)];
#endif
#if UART_MAX>1
static char s_uart1_txbuf[512+ sizeof(fifo_s)];
#endif
#if UART_MAX>2
static char s_uart2_txbuf[512+ sizeof(fifo_s)];
#endif
#endif

void isr_uart_handler(uint8 uart)
{
    if (USART_GetITStatus(s_uartInfo[uart].m_usart, USART_IT_RXNE)!=RESET)
    {
        /* 接收中断:将一个字节放入循环缓冲中 */
        fifo_putchar(s_uartInfo[uart].m_rxbuf, (char)USART_ReceiveData(s_uartInfo[uart].m_usart));
    }
    #if USE_UART_TX_INT
    if (USART_GetITStatus(s_uartInfo[uart].m_usart, USART_IT_TXE) != RESET)
    {
        /* 发送中断:通知发送任务进行发送 */
        CoPostSem(s_semUartTxReady[uart]);
    }
    #endif
}

bool uart_init(uint8 uart,uint32 baudrate)
{
    switch(uart)
    {
        #if UART_MAX>0
        case UART_COM0:
            stm32_nvic_init(NVIC_PriorityGroup_1, USART1_IRQn, 0, 1);
            stm32_usart_init((uint32)s_uartInfo[uart].m_usart, baudrate, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No);
            GPIO_INIT(GPIO_UART1_TX,GPIO_Mode_AF_PP,GPIO_Speed_50MHz);/* Tx设置为复用推挽输出,输出能力更强 */
            GPIO_INIT(GPIO_UART1_RX,GPIO_Mode_IN_FLOATING,GPIO_Speed_50MHz);/* Rx设置为浮空输入 */
            s_uartInfo[uart].m_rxbuf = s_uart0_rxbuf;
            s_uartInfo[uart].m_rxbufLen = sizeof(s_uart0_rxbuf);
            #if USE_UART_TX_INT
            s_uartInfo[uart].m_txbuf = s_uart0_txbuf;
            s_uartInfo[uart].m_txbufLen = sizeof(s_uart0_txbuf);
            #endif
            break;
        #endif
        #if UART_MAX>1
        case UART_COM1:
            stm32_nvic_init(NVIC_PriorityGroup_1, USART2_IRQn, 0, 1);
            stm32_usart_init((uint32)s_uartInfo[uart].m_usart, baudrate, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No);
            GPIO_INIT(GPIO_UART2_TX,GPIO_Mode_AF_PP,GPIO_Speed_50MHz);/* Tx设置为复用推挽输出,输出能力更强 */
            GPIO_INIT(GPIO_UART2_RX,GPIO_Mode_IN_FLOATING,GPIO_Speed_50MHz);/* Rx设置为浮空输入 */
            s_uartInfo[uart].m_rxbuf = s_uart1_rxbuf;
            s_uartInfo[uart].m_rxbufLen = sizeof(s_uart1_rxbuf);
            #if USE_UART_TX_INT
            s_uartInfo[uart].m_txbuf = s_uart1_txbuf;
            s_uartInfo[uart].m_txbufLen = sizeof(s_uart1_txbuf);
            #endif
            break;
        #endif
        #if UART_MAX>2
        case UART_COM2:
            stm32_nvic_init(NVIC_PriorityGroup_1, USART3_IRQn, 0, 1);
            stm32_usart_init((uint32)s_uartInfo[uart].m_usart, baudrate, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No);
            GPIO_INIT(GPIO_UART3_TX,GPIO_Mode_AF_PP,GPIO_Speed_50MHz);/* Tx设置为复用推挽输出,输出能力更强 */
            GPIO_INIT(GPIO_UART3_RX,GPIO_Mode_IN_FLOATING,GPIO_Speed_50MHz);/* Rx设置为浮空输入 */
            s_uartInfo[uart].m_rxbuf = s_uart2_rxbuf;
            s_uartInfo[uart].m_rxbufLen = sizeof(s_uart2_rxbuf);
            #if USE_UART_TX_INT
            s_uartInfo[uart].m_txbuf = s_uart2_txbuf;
            s_uartInfo[uart].m_txbufLen = sizeof(s_uart2_txbuf);
            #endif
            break;
        #endif
        default:
            return false;
    }
    /* 初始化接收缓存 */
    if(!fifo_init(s_uartInfo[uart].m_rxbuf,s_uartInfo[uart].m_rxbufLen))
    {
        s_uartInfo[uart].m_rxbuf = NULL;
    }
    
    #if USE_UART_TX_INT
    s_semUartTxReady[uart] = CoCreateSem(1,1,EVENT_SORT_TYPE_FIFO);
    if(!fifo_init(s_uartInfo[uart].m_txbuf,s_uartInfo[uart].m_txbufLen))
    {
        s_uartInfo[uart].m_txbuf = NULL;
    }
    #endif
    return true;
}

void uart_reset(uint32 uart)
{

}

uint32 uart_recv_byte(uint8 uart,char* ch)
{
    switch(uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_getchar(s_uartInfo[uart].m_rxbuf, ch);
    default:
        return 0;
    }
}

uint32 uart_recv(uint8 uart, char* buf, uint32 size)
{
    switch(uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_getbuf(s_uartInfo[uart].m_rxbuf, buf, size);
    default:
        return 0;
    }
}

uint32 uart_recv_line(uint8 uart, char* line,uint32 size)
{
    switch(uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_getline(s_uartInfo[uart].m_rxbuf, line, size);
    default:
        return 0;
    }
}

bool uart_send_byte_directly(uint8 uart, char ch)
{
    switch (uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        USART_SendData(s_uartInfo[uart].m_usart, ch);
        while(USART_GetFlagStatus(s_uartInfo[uart].m_usart,USART_FLAG_TXE)==RESET);
        break;
    default:
        return false;
    }
    return true;
}

bool uart_send_byte(uint8 uart, char ch)
{
    #if USE_UART_TX_INT
    switch (uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_putchar(s_uartInfo[uart].m_txbuf, ch);
    default:
        return false;
    }
    return true;
    #else
    return uart_send_byte_directly(uart, ch);
    #endif
}


uint32 uart_send_str(uint8 uart,char* str)
{   
    #if USE_UART_TX_INT
    switch (uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_putstr(s_uartInfo[uart].m_txbuf, str);
    default:
        return 0;
    }
    #else
    char* pch = str;
    while(*pch!=0)
    {
        if(!uart_send_byte(uart,*pch))
        {
            return (pch-str);
        }
        pch++;
    }
    return strlen(str);
    #endif
}

uint32 uart_send(uint8 uart, char* data, uint32 len)
{
    #if USE_UART_TX_INT
    switch (uart){
    case UART_COM0:
    case UART_COM1:
    case UART_COM2:
        return fifo_putbuf(s_uartInfo[uart].m_txbuf, data, len);
    default:
        return 0;
    }
    #else
    uint32 i;
    for(i=0; i<len; i++)
    {
        if(!uart_send_byte(uart,*(data+i)))
        {
            return i;
        }
    }
    return len;
    #endif
}
#if USE_UART_TX_INT
void task_uart_tx_polling(void *pData)
{
    uint8 i;
    UNUSED_PARAM(pData);
    while(1)
    {
        msleep(10);
        for(i=0; i<UART_MAX; i++)
        {
            if(((fifo_s *)s_uartInfo[i].m_txbuf)->entry > 0)
            {
                if (CoAcceptSem(s_semUartTxReady[i])>0)/* 发送空闲 */
                {
                    /* 取出数据进行发送 */
                    char ch;
                	uint8 len = fifo_getchar(s_uartInfo[i].m_txbuf,&ch);
                    if(len==1)
                	{
                        uart_send_byte_directly(i,len);
                	}
                }
            }
        }
    }
}
#endif


