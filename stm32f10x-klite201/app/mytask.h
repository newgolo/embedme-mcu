/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MY_TASK_H__
#define __MY_TASK_H__

#include "kernel.h"
#include "system.h"

#define APP_VERSION  "STM32 V1.00.01" 
#define APP_DEBUG    1  /* APP调试开关(用于支持额外的调试命令,正式发布版时关闭) */

/*------定义任务默认堆栈大小(实际堆栈大小=STKLEN*4,4为MCU位宽)-------*/
#define TASK_STACK_256B        64
#define TASK_STACK_512B        128
#define TASK_STACK_1024B       256
#define TASK_STACK_1536B       384
#define TASK_STACK_2048B       512
#define TASK_STACK_3072B       768
#define TASK_STACK_4096B       1024
#define TASK_STACK_MAX         4095 /* CoOS任务堆栈最大为4095(0xfff) */

void task_main(void* pdata); //主任务

#endif
