/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "pga2311.h"
#include "spi.h"

#define GAIN_MIN            (-955)     /* -95.5dB */
#define GAIN_MAX            (315)      /* 31.5dB */
#define GAIN_TO_SPI(gain)   ((uint8)((960+((uint16)gain))/5))   /* Gain = 31.5-(0.5*(255-N)),N=1~255 */   
sint16 s_gain[4]={GAIN_MIN,GAIN_MIN,GAIN_MIN,GAIN_MIN};
sint16 pga2311_get_gain(uint8 gainDev)
{
    return s_gain[gainDev];
}

bool pga2311_set_gain(uint8 gainDev,sint16 gain)
{
    uint8 spi_dev;
    switch(gainDev){
    case 0:
    case 1:
        spi_dev = SPI_DEV_PGA2311_1;
        break;
    case 2:
    case 3:
        spi_dev = SPI_DEV_PGA2311_2;
        break;
    default:
        return false;
    }
    if(!SPI_OPEN(spi_dev))
    {
        return false;
    }
    s_gain[gainDev] = CLIP(GAIN_MIN,gain,GAIN_MAX);
    switch(gainDev){
    case 0:
    case 1:
        SPI_WRITE(spi_dev,GAIN_TO_SPI(s_gain[gainDev]),2);   /* ��д������ */
        SPI_WRITE(spi_dev,GAIN_TO_SPI(s_gain[gainDev]),2);   /* ��д������ */
        break;
    case 2:
    case 3:
        SPI_WRITE(spi_dev,GAIN_TO_SPI(s_gain[gainDev]),2);   /* ��д������ */
        SPI_WRITE(spi_dev,GAIN_TO_SPI(s_gain[gainDev]),2);   /* ��д������ */
        break;
    default:
        break;
    }
    SPI_CLOSE(spi_dev);
    return true;
}

