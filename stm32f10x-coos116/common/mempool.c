/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "mempool.h"
#include "kernel.h"

/* 根据应用实际使用情况定义每种内存块的个数,至少2个 */
#define MEM_4B_BLKS      6  /* used by: InputEventMsg_S,PttMsg_S,TetraGpsMsg_S */
#define MEM_24B_BLKS     3  /* used by: SdsStatusMsg_S */
#define MEM_160B_BLKS    3  /* used by: SdsTextMsg_S */
#define MEM_280B_BLKS    3  /* used by: SdsLongMsg_S */

/* 块大小定义 */
#define MEM_4B_SIZE      4
#define MEM_24B_SIZE     24
#define MEM_160B_SIZE    160
#define MEM_280B_SIZE    280


#if MEM_4B_BLKS>1
static char s_mem_4bytes[MEM_4B_BLKS][MEM_4B_SIZE]={0};
#endif

#if MEM_24B_BLKS>1
static char s_mem_24bytes[MEM_24B_BLKS][MEM_24B_SIZE]={0};
#endif

#if MEM_160B_BLKS>1
static char s_mem_168bytes[MEM_160B_BLKS][MEM_160B_SIZE]={0};
#endif

#if MEM_280B_BLKS>1
static char s_mem_288bytes[MEM_280B_BLKS][MEM_280B_SIZE]={0};
#endif

static OS_MMID s_mempool[MEM_POOL_MAX]={0};

bool mempool_init(void)
{
    #if MEM_4B_BLKS>1
    s_mempool[MEM_POOL_4B]=CoCreateMemPartition((U8*)s_mem_4bytes, MEM_4B_SIZE, MEM_4B_BLKS);
    if(s_mempool[MEM_POOL_4B]==-1)
    {
        return false;
    }
    #endif

    #if MEM_24B_BLKS>1
    s_mempool[MEM_POOL_24B]=CoCreateMemPartition((U8*)s_mem_24bytes, MEM_24B_SIZE, MEM_24B_BLKS);
    if(s_mempool[MEM_POOL_24B]==-1)
    {
        return false;
    }
    #endif
    
    #if MEM_160B_BLKS>1
    s_mempool[MEM_POOL_160B]=CoCreateMemPartition((U8*)s_mem_168bytes, MEM_160B_SIZE, MEM_160B_BLKS);
    if(s_mempool[MEM_POOL_160B]==-1)
    {
        return false;
    }
    #endif
    
    #if MEM_280B_BLKS>1
    s_mempool[MEM_POOL_280B]=CoCreateMemPartition((U8*)s_mem_288bytes, MEM_280B_SIZE, MEM_280B_BLKS);
    if(s_mempool[MEM_POOL_280B]==-1)
    {
        return false;
    }
    #endif
    
    return true;
}

/*******************************************************
 * mempool_get_mem:
 * 从内存池获取内存
 ******************************************************/
void* mempool_get_mem(uint8 mempool)
{
    void* mem;
    if(mempool>=MEM_POOL_MAX ||
       s_mempool[mempool]==-1)
    {
        return NULL;
    }
    mem = CoGetMemoryBuffer(s_mempool[mempool]);
    if(mem!=NULL)
    {
        uint16 size=0;
        switch(mempool){
        case MEM_POOL_4B: 
            size = MEM_4B_SIZE; 
            break;
        case MEM_POOL_24B: 
            size = MEM_24B_SIZE; 
            break;  
        case MEM_POOL_160B: 
            size = MEM_160B_SIZE; 
            break; 
        case MEM_POOL_280B: 
            size = MEM_280B_SIZE; 
            break;
        default:
            break;
        }
        memset(mem,0,size);
    }
    return mem;
}

/*******************************************************
 * mempool_put_mem
 * 归还内存给内存池
 ******************************************************/
bool mempool_put_mem(uint8 mempool,void* mem)
{
    if(mempool>=MEM_POOL_MAX ||
       s_mempool[mempool]==-1)
    {
        return false;
    }
    if(E_OK!=CoFreeMemoryBuffer(s_mempool[mempool],mem))
    {
        return false;
    }
    return true;
}

