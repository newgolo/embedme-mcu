/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "board.h"
#include "flash.h"
#include "trace.h"
#include <string.h>

/**
 *  使用stm32内部flash进行存储
 *  1.使用之前必须先修改链接文件stm32f10x_flash.icf,增加一段地址用于flash存储:
 *  define block CSTACK     with alignment = 8, size = __ICFEDIT_size_cstack__   { };
 *  define block HEAP       with alignment = 8, size = __ICFEDIT_size_heap__     { };
 *  define block FSPAGE     with alignment = 8, size = 0x400     { };
 *  ......
 *  place at end of ROM_region  {block FSPAGE };
 *  增加FSPAGE段(页大小为1024byte),并放置在ROM的尾部,即第63页.用于存储配置信息.
 *  STM32F10x大容量产品页大小为2K,共256页,中小容量页大小为1K.存储区起始地址为0x08000000.
 *  2.在程序中访问,直接访问该地址:
 *  s_FLASH_PAGE_START = (uint32_t)__section_begin("FSPAGE");
 *
 */
#pragma section = "FSPAGE"
#define PAGE_SIZE   1024
static uint32 s_FLASH_PAGE_START; 

void flash_init()
{
    s_FLASH_PAGE_START = (uint32)__section_begin("FSPAGE");
}

static bool flash_disable_write_protect()
{
    uint32 start_page_address=s_FLASH_PAGE_START;
    uint32 userOptionByte = 0;
    uint32 mask = 0;
    uint32 writeProtectFlags = 0;
    uint16 ob_iwdg = OB_IWDG_SW;
    uint16 ob_stop = OB_STOP_NoRST;
    uint16 ob_stdby = OB_STDBY_NoRST;
    FLASH_Status status = FLASH_BUSY;

    /* 写保护选择字节只有4字节,大容量以2页(2*1024)为单位,中小容量以4页(4*1024)为单位 */
    mask = 1 << ((start_page_address - 0x08000000) >> 12);   /* we use only one page ;-) */ 
    writeProtectFlags = FLASH_GetWriteProtectionOptionByte();/* 获取写保护字节寄存器值 */
    if ((writeProtectFlags & mask) != mask)/* 如果有写保护,则关闭 */
    {
        userOptionByte = FLASH_GetUserOptionByte();
        mask |= writeProtectFlags;
        status = FLASH_EraseOptionBytes();
        if (mask != 0xFFFFFFFF)
        {
            status = FLASH_EnableWriteProtection((uint32)~mask);
        }

        if ((userOptionByte & 0x07) != 0x07)/* 测试用户选项字节是否已编程 */
        {
            if ((userOptionByte & 0x01) == 0x0)
            {
                ob_iwdg = OB_IWDG_HW;
            }
            if ((userOptionByte & 0x02) == 0x0)
            {
                ob_stop = OB_STOP_RST;
            }
            if ((userOptionByte & 0x04) == 0x0)
            {
                ob_stdby = OB_STDBY_RST;
            }
            FLASH_UserOptionByteConfig(ob_iwdg, ob_stop, ob_stdby);
        }

        if (status == FLASH_COMPLETE)
        {
            TRACE_WAR("flash WRPR disabled.");
            TRACE_WAR("system reset to re-load new option bytes.");
            RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);/* 使能看门狗 */

            /* Generate a system Reset to re-load the new option bytes: enable WWDG and set*
             * counter value to 0x4F, as T6 bit is cleared this will generate a WWDG reset */
            WWDG_Enable(0x4F);
        }
        else
        {
            TRACE_ERR("flash disable WRPR failed!");
            return false;
        }
    }
    return true;
}
uint32 flash_page_read(uint32 page,void* buf,uint32 size)
{
    if (size>PAGE_SIZE)
    {
        size = PAGE_SIZE;
    }
    memcpy(buf, (void *)(s_FLASH_PAGE_START+(page<<10)), size);
    return size;
}

uint32 flash_page_write(uint32 page,void* buf,uint32 size)
{
    uint32 buf_addr = (uint32)buf;
    uint32 page_addr = s_FLASH_PAGE_START+(page<<10);
    uint32 retval;
    FLASH_Unlock();

    if(!flash_disable_write_protect())
    {
        FLASH_Lock();
        return 0;
    }

    size = size-(size%4);
    if (size>PAGE_SIZE)
    {
        size = PAGE_SIZE;
    }
    
    /* 擦除页面 */
    if (FLASH_ErasePage(page_addr) != FLASH_COMPLETE)
    {
        TRACE_ERR("Flash_ErasePage error!");
        FLASH_Lock();
        return 0;
    }
    retval = size;
    while (size > 0)
    {
        FLASH_ProgramWord(page_addr, *(uint32*)buf_addr);
        page_addr += 4;
        buf_addr += 4;
        size -= 4;
    }
    FLASH_Lock();
    return retval;
}

