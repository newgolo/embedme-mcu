/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MYLIST_H__
#define __MYLIST_H__

#include "basetype.h" 

typedef struct _link_s{
    struct _link_s* m_prev;
    struct _link_s* m_next;
}link_s;

/***************************************************
 list的实现参照linux内核双向循环链表:
 将link_s嵌入至数据结构中构成结点数据从而形成链表,
 双向循环链表的任意一个结点都可以可当作头结点.
 **************************************************/
typedef link_s list_s;  /* prefer to list */ 
typedef link_s node_s;  /* prefer to node */

#define LIST_INIT_LINK(node)    {(node).m_prev=&(node);(node).m_next=&(node);}

#define LIST_ENTRY(link_ptr, container_type, link_name)    \
        ((container_type *)((char *)(link_ptr) - (uint32)(&((container_type *)0)->link_name)))

/* 遍历除head自身外的其他在同一链表中的元素,head为已知参数,不需要再遍历了 */
#define LIST_FOR_OTHERS(iterator, head)      \
        for((iterator)=(head)->m_next; (iterator)!=(head); (iterator)=(iterator)->m_next)

/* list api */
void list_add(node_s *pnode, list_s *head);
void list_add_tail(node_s *node, list_s *head);
void list_del(node_s *entry);
bool list_is_empty(const list_s *head);
#endif
